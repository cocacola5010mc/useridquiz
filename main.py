from functions import *

first = generateDoWhile(isNotQuit, generateDoWhile(isNotEmptyString, inputWrapper('Input your first name >>> ')))()
last = generateDoWhile(isNotQuit, generateDoWhile(isNotEmptyString, inputWrapper('Input your last name >>> ')))()
[day, month, year] = generateDoWhile(isDate, generateDoWhile(isNotQuit, generateDoWhile( isNotEmptyString, inputWrapper('Input a date (DD/MM/YYYY) >>> '))))().split('/')

userId = makeUserId(first, last, day, month, year)

print(userId)
