from functions import *

def test (prompt, expectation, function, *functionArgs):
    print(prompt)
    output = function(*functionArgs)
    if output == expectation:
        print("\033[32mSuccess\033[0m")
    else:
        print("\033[91mFunction: " + function.__name__ + " failed to run\033[0m")

test('Testing is not empty string with string with characters', True, isNotEmptyString, 'a string')
test('Testing is not empty string with string with empty string', False, isNotEmptyString, '')
test('Testing is not quit with string that isnt quit', True, isNotQuit, 'anrseito')
test('Testing is date with invalid format', False, isDate, 'arnsetio')
test('Testing is date with letters rather than numbers', False, isDate, 'DD/MM/YYYY')
test('Testing is date with correct format and numbers', True, isDate, '28/06/2020')
