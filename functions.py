from sys import exit

def generateDoWhile (testFn, valueFn):
    def fn ():
        value = valueFn()
        if testFn(value):
            return value
        else:
            return fn ()
    return fn

def isNotEmptyString (string):
    if string == '':
        return False
    else:
        return True 

def isNotQuit (string):
    if string == 'quit':
        print('Quitting')
        exit(0)
    else:
        return True 

def isDate (string):
    dateArr = string.split('/')

    def isCorrectDigit (digitString, length):
        return digitString.isdigit() and len(digitString) == length

    if len(dateArr) == 3:
        [day, month, year] = dateArr
        return isCorrectDigit(day, 2) and isCorrectDigit(month, 2) and isCorrectDigit(year, 4)
    else:
        return False

def inputWrapper (message):
    def fn ():
        return input(message)
    return fn

def makeUserId (first, last, day, month, year):
    return (year + month + day + last[:3] + first[0] + str(len(first))).upper()
